describe "POST/signup" do
  context "novo usuario" do
    before(:all) do
      payload = { name: "Lucas", email: "lucas@gmail.com", password: "123.teste" }
      MongoDB.new.remove_user(payload[:email])

      @result = Signup.new.create(payload)
    end

    it "valida status code" do
      expect(@result.code).to eql 200
    end

    it "valida id do usuário" do
      expect(@result.parsed_response["_id"].length).to eql 24
    end
  end

  context "usuario ja existe" do
    before(:all) do
      # dado que tenho um novo usuário
      payload = { name: "Paulo", email: "paulo@gmail.com", password: "123.teste" }
      MongoDB.new.remove_user(payload[:email])

      # e o email desse usuário já foi cadastrado no sistema
      Signup.new.create(payload)

      #quando faço uma requisição para a rota /signup
      @result = Signup.new.create(payload)
    end

    it "deve retornar 409" do
      # então deve retornar 409
      expect(@result.code).to eql 409
    end

    it "deve retornar mensagem" do
      expect(@result.parsed_response["error"]).to eql "Email already exists :("
    end
  end

  #nome é Obrigatório
  #email é obrigatório
  #password é obrigatório

  examples = [
    {
      title: "Sem nome",
      payload: { name: "", email: "email@gmail.com", password: "123.321" },
      code: 412,
      error: "required name",
    },

    {
      title: "Sem email",
      payload: { name: "Nome qualquer", email: "", password: "123.321" },
      code: 412,
      error: "required email",
    },

    {
      title: "Sem senha",
      payload: { name: "Nome qualquer", email: "email@gmail.com", password: "" },
      code: 412,
      error: "required password",
    },
  ]

  examples.each do |e|
    context "#{e[:title]}" do
      before(:all) do
        @result = Signup.new.create(e[(:payload)])
      end

      it "valida status code #{e[:code]}" do
        expect(@result.code).to eql e[:code]
      end

      it "valida id do usuário" do
        expect(@result.parsed_response["error"]).to eql e[:error]
      end
    end
  end
end
