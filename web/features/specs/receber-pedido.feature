#language:pt

Funcionalidade: Receber pedido de locação
    Sendo um anunciante que possui equipamentos cadastrados
    Desejo receber pedidos de locação
    Para que eu possa decicir se quero aprova-los ou rejeita-los

    Cenario: Receber Pedido

        Dado que meu perfil de anunciante é "joao@gmail.com" e "123.teste"
            E que eu tenho o seguinte equipamento cadastrado:
            | thumb     | trompete.jpg |
            | nome      | Trompete     |
            | categoria | Outros       |
            | preco     | 100          |
            E acesso o meu dashboard
        Quando "maria@gmail.com" e "123.teste" solicita a locação desse equipo
        Então devo ver a seguinte mensagem:
            """
            maria@gmail.com deseja alugar o equipamento: Trompete em: DATA_ATUAL
            """
            E devo ver os links: "ACEITAR" e "REJEITAR" no pedido
