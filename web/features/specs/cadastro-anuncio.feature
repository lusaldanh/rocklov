#language: pt

Funcionalidade: Cadastro de Anúncios
    Sendo usuário cadastrado no Rocklov que possui equipamentos musicais
    Quero cadastrar meus equipamentos
    Para que eu possa disponibiliza-los para locação

    Contexto: Login
        * Login com "anna@gmail.com" e "123.teste"

    Cenario: Novo equipo

        Dado que acesso o formulario de cadastro de anúncios
            E que eu tenho o seguinte equipamento:
            | thumb     | fender-sb.jpg |
            | nome      | Fender Strato |
            | categoria | Cordas        |
            | preco     | 200           |
        Quando submetos o cadastro desse item
        Então devo ver esse item no meu Dashboard

        Esquema do Cenario: Tentativa de cadastro de anuncio
        Dado que acesso o formulario de cadastro de anúncios
            E que eu tenho o seguinte equipamento:
            | thumb     | <foto>      |
            | nome      | <nome>      |
            | categoria | <categoria> |
            | preco     | <preco>     |
        Quando submetos o cadastro desse item
        Então deve conter a mensagem de alerta: "<saida>"

        Exemplos:
            | foto             | nome      | categoria | preco | saida                                |
            |                  | Baixo     | Cordas    | 100   | Adicione uma foto no seu anúncio!    |
            | violao-nylon.jpg |           | Cordas    | 150   | Informe a descrição do anúncio!      |
            | mic.jpg          | MIcrofone |           | 50    | Informe a categoria                  |
            | mic.jpg          | Microfone | Outros    |       | Informe o valor da diária            |
            | conga.jpg        | Conga     | Outros    | abc   | O valor da diária deve ser numérico! |
            | conga.jpg        | Conga     | Outros    | 100a  | O valor da diária deve ser numérico! |

