#language: pt

Funcionalidade: Login
    Sendo um usuário cadastrado
    Quero acessar o sistema da Rocklov
    Para que eu possa anunciar meus equipamentos musicais

    @login
    Cenario: Login do usuário

        Dado que acesso a página principal
        Quando submeto minhas credenciais com "lucy@gmail.com" e "123.teste"
        Então sou redirecionado para o Dashboard

    Esquema do Cenario: Tentar logar
        Dado que acesso a página principal
        Quando submeto minhas credenciais com "<email_input>" e "<senha_input>"
        Então vejo a mensagem de alerta: "<mensagem_output>"

        Exemplos:
            | email_input          | senha_input | mensagem_output                  |
            | luciane@gmail.com    | 123456      | Usuário e/ou senha inválidos.    |
            | outroemail@gmail.com | 123.teste   | Usuário e/ou senha inválidos.    |
            | luciane*gmail.com    | 123456      | Oops. Informe um email válido!   |
            |                      | 123456      | Oops. Informe um email válido!   |
            | luciane@gmail.com    |             | Oops. Informe sua senha secreta! |

